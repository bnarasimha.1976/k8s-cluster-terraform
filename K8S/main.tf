# Authentication to AWS from Terraform code 
provider "aws" {
    region = "us-east-1"
    profile = "default"
    version = "~>2.15.0"
}
# Create a VPC in AWS part of NV region
resource "aws_vpc" "kube_vpc" {
    cidr_block = "10.0.0.0/16"
    instance_tenancy = "default"
    enable_dns_hostnames = true
    enable_dns_support = true

        tags {
            Name = "kube_vpc"
            Created_By = "Terraform"
        }
}
# Create a Public Subnet1 part of kube_vpc
resource "aws_subnet" "kube_public_subnet1"{
    vpc_id = aws_vpc.kube_vpc.id
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = true
    availability_zone = "us-east-1a"

    tags {
        Name = "kube_public_subnet1"
        Created_By = "Terraform"
    }
}
#Create a Public Subnet2 part of kube_vpc
resource "aws_subnet" "kube_public_subnet2" {
    vpc_id = aws_vpc.kube_vpc.id
    cidr_block = "10.0.2.0/24"
    map_public_ip_on_launch = true
    availability_zone = "us-east-1b"

    tags {
        Name = "kube_public_subnet2"
        Created_By = "Terraform"
    }
}

#Create a Internet Gateway and Map to VPC
resource "aws_internet_gateway" "kube_internet_gateway"{
    vpc_id = aws_vpc.kube_vpc.id

    tags {
        Name = "kube_internet_gateway"
        Created_By = "Terraform"
    }
}
#Create a Route Table Under VPC
resource "aws_route_table" "kube_route_table"{
    vpc_id = aws_vpc.kube_vpc.id

    tags {
        Name = "kube_route_table"
        Created_By = "Terraform"
    }
}
# Create the internet Access 
resource "aws_route" "my_rtb_igw" {
    route_table_id = "${aws_route_table.my_route_table.id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.my_internet_gateway.id}"
}
# Associate Public Subnets with Route Tables
resource "aws_route_table_association" "my_subnet_association1" {
    subnet_id = "${aws_subnet.my_public_subnet1.id}"
    route_table_id = "${aws_route_table.my_route_table.id}"
}
# Associate Public Subnets with Route Tables
resource "aws_route_table_association" "my_subnet_association2" {
    subnet_id = "${aws_subnet.my_public_subnet2.id}"
    route_table_id = "${aws_route_table.my_route_table.id}"
}
# WebServer - RDP - SSH - HTTP Security group
resource "aws_security_group" "my_k8s_sg" {
    vpc_id = aws_vpc.my_vpc.id
    name   = "my_web_sg"
    description = "To Allow SSH-RDP-HTTP From IPV4 Devices"

    # Allow Ingress / inbound Of port 22 
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 22
        to_port     = 22 
        protocol    = "tcp"
    }
    # Allow Ingress / inbound Of port 3389
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 3389
        to_port     = 3389
        protocol    = "tcp"
    }
        # Allow Ingress / inbound Of port 8080
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 8080
        to_port     = 8080
        protocol    = "tcp"
    }
    # Allow Ingress / inbound Of port 8080
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
    }
    # Allow egress / outbound of all ports 
    egress {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
        # Allow Ingress / inbound Of port 6443*
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 6443
        to_port     = 6443
        protocol    = "tcp"
    }
            # Allow Ingress / inbound Of port 2379-2380
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 2379
        to_port     = 2380
        protocol    = "tcp"
    }
      # Allow Ingress / inbound Of port 10250-10252
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 10250
        to_port     = 10252
        protocol    = "tcp"
    }
    tags = {
        Name = "my_k8s_sg"
        Description = "kube allow SSH - RDP - HTTP"
        Createdby = "terraform"
    }
}
# Create SSH Keys using your local keys
resource "aws_key_pair" "k8s_keys" {
    key_name = "kube_keys"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDDcj1TlEW1fiBo+GZK/s/ATZfNGFdfwUyntkBRSt1LM83CJccpXA6N5Vbv50s8kkgTC50G/1Kc39XxZaYuzwrnKgLLNTLVpFPQFr0euauxkT6K/KtR1QN+KCFb2KOY8eGltoxqbMY8YFkMGf47lr64Caa84v/eI+gtUkx7/eCtIM4SFElDYCZOfmOLfZjhKieUkSzuorbINY0WWQR+kXGYW7RcX36FBKnxhykaNid8Zv74kPQmO0w8VKoLmZxb7r2qb/PwvODY2ifxLIpBRUiGoUNak2N/3H6JfZ7BD7+9mAV4dtgVZhHJ5Lp2MMYIoG0ppvCy8+S//gZKRDCCYcenAJ1WpRCRR7AMzLHlR9Kdcj08DLZip9YOvCYBXs1L/yppYGeanKr0tsWLbgYPZAGcVf3mexLo5gd6qarb9Qm8MlOX2QXFq8vMCtiSND8TbE++cFrar1ok9CaT+N29slIPg7K224pjcJYNLtACpvj0WHTFP986zwwqkfRHLpf3OAk= User@Chris-eCom"
}

 # Create k8s-master
 resource "aws_instance" "k8s-master"{
     ami = "ami-0747bdcabd34c712a"
     instance_type = "t2.medium"
     count =  3
     key_name = "k8s_keys"
     subnet_id = aws_subnet.my_public_subnet1.id
     vpc_security_group_ids = ["${aws_security_group.my_k8s_sg.id}"]
     
     tags = {
         Name = "k8s-master"
         Created_By = "Terraform"
     }    
}
