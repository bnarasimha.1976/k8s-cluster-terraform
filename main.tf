# Authentication to AWS from Terraform code 
provider "aws" {
    region = "us-east-1"
    profile = "default"
}

# Create a VPC in AWS part of NV region
resource "aws_vpc" "k8s_vpc" {
    cidr_block = "10.0.0.0/16"
    instance_tenancy = "default"
    enable_dns_hostnames = true
    enable_dns_support = true

        tags = {
            Name = "k8s_vpc"
            Created_By = "Terraform"
        }
}
# Create a Public Subnet1 part of my_vpc
resource "aws_subnet" "k8s_public_subnet1"{
    vpc_id = "${aws_vpc.k8s_vpc.id}"
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = true
    availability_zone = "us-east-1a"

    tags = {
        Name = "k8s_public_subnet1"
        Created_By = "Terraform"
    }
}
#Create a Public Subnet2 part of my_vpc
resource "aws_subnet" "k8s_public_subnet2" {
    vpc_id = "${aws_vpc.k8s_vpc.id}"
    cidr_block = "10.0.2.0/24"
    map_public_ip_on_launch = true
    availability_zone = "us-east-1b"

    tags = {
        Name = "k8s_public_subnet2"
        Created_By = "Terraform"
    }
}

#Create a Internet Gateway and Map to VPC
resource "aws_internet_gateway" "k8s_internet_gateway"{
    vpc_id = "${aws_vpc.k8s_vpc.id}"

    tags = {
        Name = "k8s_internet_gateway"
        Created_By = "Terraform"
    }
}
#Create a Route Table Under VPC
resource "aws_route_table" "k8s_route_table"{
    vpc_id = "${aws_vpc.k8s_vpc.id}"

    tags = {
        Name = "k8s_route_table"
        Created_By = "Terraform"
    }
}
# Create the internet Access 
resource "aws_route" "k8s_rtb_igw" {
    route_table_id = "${aws_route_table.k8s_route_table.id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.k8s_internet_gateway.id}"
}
# Associate Public Subnets with Route Tables
resource "aws_route_table_association" "k8s_subnet_association1" {
    subnet_id = "${aws_subnet.k8s_public_subnet1.id}"
    route_table_id = "${aws_route_table.k8s_route_table.id}"
}
# Associate Public Subnets with Route Tables
resource "aws_route_table_association" "k8s_subnet_association2" {
    subnet_id = "${aws_subnet.k8s_public_subnet2.id}"
    route_table_id = "${aws_route_table.k8s_route_table.id}"
}
# WebServer - RDP - SSH - HTTP Security group
resource "aws_security_group" "kubernetes_k8s_sg" {
    vpc_id = "${aws_vpc.k8s_vpc.id}"
    name   = "kubernetes_k8s_sg"
    description = "To Allow SSH-RDP-HTTP From IPV4 Devices"

    # Allow Ingress / inbound Of port 22 
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 22
        to_port     = 22 
        protocol    = "tcp"
    }
    # Allow Ingress / inbound Of port 3389
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 3389
        to_port     = 3389
        protocol    = "tcp"
    }
        # Allow Ingress / inbound Of port 8080
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 8080
        to_port     = 8080
        protocol    = "tcp"
    }
    # Allow Ingress / inbound Of port 8080
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
    }
    # Allow egress / outbound of all ports 
    egress {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
        # Allow Ingress / inbound Of port 6443*
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 6443
        to_port     = 6443
        protocol    = "tcp"
    }
            # Allow Ingress / inbound Of port 2379-2380
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 2379
        to_port     = 2380
        protocol    = "tcp"
    }
      # Allow Ingress / inbound Of port 10250-10252
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 10250
        to_port     = 10252
        protocol    = "tcp"
    }
    tags = {
        Name = "my_k8s_sg"
        Description = "MY allow SSH - RDP - HTTP"
        Createdby = "terraform"
    }
}
# Create SSH Keys using your local keys
resource "aws_key_pair" "k8s_key" {
    key_name = "k8s"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC3LHD9V3iMGBbUOGUMBpskCE95yyrbQTVWAkvCtF6OICJFzK1KFRtLd+QcKaejhnFBMBd2dh7Xl9g4dvUXH+sxelnf2kGRrdoi8NeECovWZtifq5Bm4ytNZaGFkmeX8VFGjqQAjEylerHQ3zRjtbIBUHISiN4lBDeRI3ZVRWjYtULl9bAogAINBKNiuTNqhOFANTJOMNRx4xPtCVUlvs/xNsL+uHdERlQE40p56ZizPxSXIgPO3TIl6FKO7wglk0U8Jga+B3+jZkl+akXWPEh6msdTW+6aUtQOGSOUHhJ+gyrDzfbyTuFdN3b9S4CK0u+vTU3kjHZ3iXgExn+j09ANmy/K4ZXqjq2iXC5pG8YVCQBuzAyaBcU2szDSabLmMD/8qMCzdrL5TpvKVPJsFRkvyaxX55mcmIDk3wRS2WJVy0KK5JLLdHSMwg7JMq6OzMoyhZV+mZJZH46Q+yYbbFWh7sagyUm5TAJ/glrpJn6Ku1z/sfgLjJXTz3QdM0ISGME= User@Chris-eCom"
}

# Create k8s-master
resource "aws_instance" "kubernetes_k8s-master" {
    ami = "ami-0747bdcabd34c712a"
    instance_type = "t2.micro"
    key_name = "k8s_key"
    subnet_id = "${aws_subnet.k8s_public_subnet1.id}"
    vpc_security_group_ids = ["${aws_security_group.kubernetes_k8s_sg.id}"]

    tags = {
        Name = "K8S Master"
        CreatedBy = "Terraform"
    }
}
# Create node-1
resource "aws_instance" "kubernetes_k8s-node1" {
    ami = "ami-0747bdcabd34c712a"
    instance_type = "t2.micro"
    key_name = "k8s_key"
    subnet_id = "${aws_subnet.k8s_public_subnet1.id}"
    vpc_security_group_ids = ["${aws_security_group.kubernetes_k8s_sg.id}"]

    tags = {
        Name = "Kubenetes Node1"
        CreatedBy = "Terraform"
    }
}
# Create node-2
resource "aws_instance" "kubernetes_k8s-node2" {
    ami = "ami-0747bdcabd34c712a"
    instance_type = "t2.micro"
    key_name = "k8s.key"
    subnet_id = "${aws_subnet.k8s_public_subnet1.id}"
    vpc_security_group_ids = ["${aws_security_group.kubernetes_k8s_sg.id}"]

    tags = {
        Name = "Kubenetes Node2"
        CreatedBy = "Terraform"
    }
}
