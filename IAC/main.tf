#Create VPC 
resource "aws_vpc" "sitla_vpc" {
  cidr_block       = var.cidr_block

  tags = {
    project = "Sitla"
    department = "Automation"
  }
}

#Create Internet Gateway under new VPC 
resource "aws_internet_gateway" "sitla_igw" {
  vpc_id = aws_vpc.sitla_vpc.id

  tags = {
    project = "Sitla"
    department = "Automation"
  }
}
#Create Public and Private Subnet
resource "aws_subnet" "publicsubnet" {
  vpc_id     = aws_vpc.sitla_vpc.id
  cidr_block = var.public_subnet_cidr
  map_public_ip_on_launch = "true"

  tags = {
    project = "Sitla"
    department = "Automation"
  }
}

resource "aws_subnet" "privatesubnet" {
  vpc_id     = aws_vpc.sitla_vpc.id
  cidr_block =  var.private_subnet_cidr

  tags = {
    project = "Sitla"
    department = "Automation"
  }
}
#Create NAT Gateway under EIP and Private Subnet
resource "aws_nat_gateway" "sitla_ngw" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.privatesubnet.id

  tags = {
    project = "Sitla"
    department = "Automation"
  }
  depends_on = [aws_internet_gateway.sitla_igw]
}
#Create EIP depends on internet gateway
resource "aws_eip" "nat_eip" {
  vpc = true
  depends_on = [aws_internet_gateway.sitla_igw]
}
#Create Private Route Table
resource "aws_route_table" "private_rtb" {
  vpc_id = aws_vpc.sitla_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.sitla_ngw.id
  }

  tags = {
    project = "Sitla"
    department = "Automation"
  }
}
#Create Public Route Table
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.sitla_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.sitla_igw.id
  }

  tags = {
    project = "Sitla"
    department = "Automation"
  }
}
resource "aws_security_group" "sitla_k8s_sg" {
    vpc_id = aws_vpc.sitla_vpc.id
    name   = "sitla_k8s_sg"
    description = "To Allow SSH-RDP-HTTP From IPV4 Devices"

    # Allow Ingress / inbound Of port 22 
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 22
        to_port     = 22 
        protocol    = "tcp"
    }
    # Allow Ingress / inbound Of port 3389
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 3389
        to_port     = 3389
        protocol    = "tcp"
    }
        # Allow Ingress / inbound Of port 8080
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 8080
        to_port     = 8080
        protocol    = "tcp"
    }
    # Allow Ingress / inbound Of port 8080
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
    }
    # Allow egress / outbound of all ports 
    egress {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
        # Allow Ingress / inbound Of port 6443*
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 6443
        to_port     = 6443
        protocol    = "tcp"
    }
    # Allow Ingress / inbound Of port 2379-2380
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 2379
        to_port     = 2380
        protocol    = "tcp"
    }
      # Allow Ingress / inbound Of port 10250-10252
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 10250
        to_port     = 10252
        protocol    = "tcp"
    }
    tags = {
        Name = "sitla_k8s_sg"
        Description = "MY allow SSH - RDP - HTTP"
        Createdby = "Terraform"
    }
}
# Create SSH Keys using your local keys
resource "aws_key_pair" "kube_keys" {
    key_name = "kube_keys"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDDcj1TlEW1fiBo+GZK/s/ATZfNGFdfwUyntkBRSt1LM83CJccpXA6N5Vbv50s8kkgTC50G/1Kc39XxZaYuzwrnKgLLNTLVpFPQFr0euauxkT6K/KtR1QN+KCFb2KOY8eGltoxqbMY8YFkMGf47lr64Caa84v/eI+gtUkx7/eCtIM4SFElDYCZOfmOLfZjhKieUkSzuorbINY0WWQR+kXGYW7RcX36FBKnxhykaNid8Zv74kPQmO0w8VKoLmZxb7r2qb/PwvODY2ifxLIpBRUiGoUNak2N/3H6JfZ7BD7+9mAV4dtgVZhHJ5Lp2MMYIoG0ppvCy8+S//gZKRDCCYcenAJ1WpRCRR7AMzLHlR9Kdcj08DLZip9YOvCYBXs1L/yppYGeanKr0tsWLbgYPZAGcVf3mexLo5gd6qarb9Qm8MlOX2QXFq8vMCtiSND8TbE++cFrar1ok9CaT+N29slIPg7K224pjcJYNLtACpvj0WHTFP986zwwqkfRHLpf3OAk= User@Chris-eCom"
}
# Create k8s-master
resource "aws_instance" "kubernetes_k8s-master" {
    ami = "ami-01179b7877e4e11b3"
    instance_type = "t2.medium"
    key_name = "kube_keys"
    subnet_id = aws_subnet.publicsubnet.id
    vpc_security_group_ids = [aws_security_group.sitla_k8s_sg.id]

    tags = {
        Name = "K8S Master"
        CreatedBy = "Terraform"
    }
}
# Create node-1
resource "aws_instance" "kubernetes_k8s-node1" {
    ami = "ami-0f8fcbe14ad4a6d5d"
    instance_type = "t2.medium"
    key_name = "kube_keys"
    subnet_id = aws_subnet.publicsubnet.id
    vpc_security_group_ids = [aws_security_group.sitla_k8s_sg.id]

    tags = {
        Name = "Kubenetes Node1"
        CreatedBy = "Terraform"
    }
}
# Create node-2
resource "aws_instance" "kubernetes_k8s-node2" {
    ami = "ami-0f8fcbe14ad4a6d5d"
    instance_type = "t2.medium"
    key_name = "kube_keys"
    subnet_id = aws_subnet.publicsubnet.id
    vpc_security_group_ids = [aws_security_group.sitla_k8s_sg.id]

    tags = {
        Name = "Kubenetes Node2"
        CreatedBy = "Terraform"
    }
}