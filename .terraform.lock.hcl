# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "2.15.0"
  constraints = "~> 2.15.0"
  hashes = [
    "h1:twJya9N4PuedoQih01tqAcyQjCe/ozCEhnuI8C7Cc+0=",
    "zh:129557c055a0ae67cf09f0c611fa9d5371c4735d8fad94667c428f9841c068b7",
    "zh:21ca2a9f6d10880fd98d3110ce64cca8fe22d0b4eff527710263498cdb37777f",
    "zh:407ec4ad9e05558e01601595499570aedf7bc56716701100ed1f5e00ca64cdf4",
    "zh:7fb9d55cb285a9563753120d1f605a59a76a559a5f487e5b2e82405d4f316c09",
    "zh:9bf4d1f5b41f25190aa3880e5fb15e380f4b158b534fdb63248031bbdd158e08",
    "zh:9ec1d7f566aa09795a3112bcc20d9cd565fefe6b2b6d364437b0732193fd4e0b",
    "zh:a9666c548dc24e97601d50a455d77f0e9fec6cec52d22317cf9e7d4b1d9bb984",
    "zh:bdf3bfae683e99c32062eec07fbc73eecc41c8e971de021d92adcd91a9a01133",
    "zh:ddefced02ba505bfdd12acf0d039bb037d269051e8250d0814182f3b221a9aae",
    "zh:e9fe2a4fd6b24f8534aa78a0e244ac639ca89a76e728aeaccfebde8dbf5672d1",
    "zh:ee97d83f039e2302c116e929242ad3c358f949a0121d5264cfcebed62aae5144",
    "zh:fd5d1175dc6dd30ae48c107c62139c7a0b7536e10539b35c3051236b222dcc5d",
  ]
}
